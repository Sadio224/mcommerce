INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (0, 'Bougie fonctionnant au feu', 'bougie qui fonctionne comme une ampoule mais sans électricité !', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1308/bialasiewicz130800160/21363428-un-magasin-plein-de-canap%C3%A9s-et-armoires.jpg', 22.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (1, 'Chaise pour s''assoire', 'Chaise rare avec non pas 1 ni 2 mais 3 pieds', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1510/bialasiewicz151000584/49423204-meubles-trendy-dans-un-petit-salon-confortable.jpg', 95.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (2, 'Cheval pour nains', 'Ce cheval ne portera certainement pas blanche neige, mais sans problème les nains', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1510/bialasiewicz151000584/49423204-meubles-trendy-dans-un-petit-salon-confortable.jpg', 360.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (3, 'Coq of steel, le superman des volailles', 'Ne passe pas au four', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1308/bialasiewicz130800160/21363428-un-magasin-plein-de-canap%C3%A9s-et-armoires.jpg', 620.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (4, 'Flacon à frotter avec un génie dedans', 'Vous donne droit à l''équivalent de 3/0 voeux', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1510/bialasiewicz151000584/49423204-meubles-trendy-dans-un-petit-salon-confortable.jpg', 1200.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (5, 'Horloge quantique', 'Donne l''heure, les minutes et même les secondes. Ne fait pas de café', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1308/bialasiewicz130800160/21363428-un-magasin-plein-de-canap%C3%A9s-et-armoires.jpg', 180.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (6, 'Table d''opération pour Hamsters', 'Pour réaliser vos opérations chirugicales sur votre Hamster!', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1510/bialasiewicz151000584/49423204-meubles-trendy-dans-un-petit-salon-confortable.jpg', 210.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (7 , 'Vase ayant appartenu a Zeus', 'Risque de choc électrique', 'https://previews.123rf.com/images/bialasiewicz/bialasiewicz1308/bialasiewicz130800160/21363428-un-magasin-plein-de-canap%C3%A9s-et-armoires.jpg', 730.0);